package users

import (
	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

type Users struct {
	gorm.Model
	ID       string `sql:"type:uuid;primary_key;default:uuid_generate_v4()`
	Username string `json:"username" gorm:"not null" binding:"required"`
	Email    string `json:"email" gorm:"not null" binding:"required"`
	Password string `json:"password" gorm:"not null" binding:"required"`
}

type ResponseFormatterRegister struct {
	ID       string `json:"id"`
	Username string `json:"username"`
	Email    string `json:"email"`
}

type LoginFormatter struct {
	ID       string `json:"id"`
	Email    string `json:"email"`
	Username string `json:"username"`
	Token    string `json:"accessToken"`
}

func LoginFormatterResponse(user LoginFormatter) LoginFormatter {
	formatter := LoginFormatter{
		ID:       user.ID,
		Username: user.Username,
		Email:    user.Email,
		Token:    user.Token,
	}
	return formatter
}

func FormatUser(user Users) ResponseFormatterRegister {
	formatter := ResponseFormatterRegister{
		ID:       user.ID,
		Username: user.Username,
		Email:    user.Email,
	}
	return formatter
}

func (user *Users) BeforeCreate(scope *gorm.DB) error {
	scope.Statement.SetColumn("ID", uuid.NewV4().String())
	return nil
}
