package service

import (
	"test_dompet/src/model/users"
	"test_dompet/src/repository"

	"golang.org/x/crypto/bcrypt"
)

type Service interface {
	RegisterUser(input users.RegisterInput) (users.Users, error)
	LoginUser(input users.LoginInput) (users.Users, error)
}

type service struct {
	repository repository.Repository
}

func NewService(repository repository.Repository) *service {
	return &service{repository}
}

func (s *service) RegisterUser(input users.RegisterInput) (users.Users, error) {
	passHash, err := bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.MinCost)
	if err != nil {
		panic(err)
	}
	student := users.Users{
		Username: input.Username,
		Email:    input.Email,
		Password: string(passHash),
	}
	newUser, err := s.repository.RegisterRepo(student)
	if err != nil {
		return newUser, err
	}
	return newUser, nil
}

func (s *service) LoginUser(input users.LoginInput) (users.Users, error) {

	student := users.Users{
		Email:    input.Email,
		Password: input.Password,
	}
	newUser, err := s.repository.Login(student)
	if err != nil {
		return newUser, err
	}
	return newUser, nil
}
