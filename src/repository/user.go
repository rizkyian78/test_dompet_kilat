package repository

import (
	"fmt"
	"test_dompet/src/model/users"

	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type repository struct {
	db *gorm.DB
}

type Repository interface {
	RegisterRepo(student users.Users) (users.Users, error)
	Login(input users.Users) (users.Users, error)
}

func NewRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (s *repository) RegisterRepo(student users.Users) (users.Users, error) {
	tx := s.db.Session(&gorm.Session{SkipDefaultTransaction: true})
	err := tx.Create(&student).Error
	if err != nil {
		return student, err
	}
	return student, nil
}

func (s *repository) Login(student users.Users) (users.Users, error) {
	inputPassword := student.Password
	tx := s.db.Session(&gorm.Session{SkipDefaultTransaction: true})
	err := tx.Where("email = ?", student.Email).Find(&student).Error
	if err != nil {
		return student, err
	}
	fmt.Println([]byte(student.Password), []byte(inputPassword))
	err = bcrypt.CompareHashAndPassword([]byte(student.Password), []byte(inputPassword))
	if err != nil {
		return student, err
	}
	return student, nil
}
