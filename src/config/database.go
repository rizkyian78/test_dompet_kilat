package config

import (
	"fmt"
	"os"
	"test_dompet/src/model/users"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

type ConfigDatabase struct {
	Database string
	Host     int
	Username string
	Password string
}

func Init() *gorm.DB {
	username := os.Getenv("USERNAME")
	password := os.Getenv("PASSWORD")
	dbHost := os.Getenv("PORT_DB")
	dbName := os.Getenv("DB_NAME")
	var dsn = fmt.Sprintf("%s:%s@tcp(127.0.0.1:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", username, password, dbHost, dbName)
	var db, err = gorm.Open(mysql.New(mysql.Config{
		DSN:                       dsn,
		DefaultStringSize:         256,
		DisableDatetimePrecision:  true,
		DontSupportRenameIndex:    true,
		DontSupportRenameColumn:   true,
		SkipInitializeWithVersion: false,
	}), &gorm.Config{
		SkipDefaultTransaction: true,
		Logger:                 logger.Default,
	})
	if err != nil {
		panic(err)
	}
	fmt.Println("database connected")
	db.AutoMigrate(users.Users{})
	return db
}
