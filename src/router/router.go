package router

import (
	"log"
	"test_dompet/src/config"
	"test_dompet/src/handler"
	"test_dompet/src/middleware"
	"test_dompet/src/repository"
	"test_dompet/src/service"

	"github.com/gin-gonic/gin"
)

func Route() {
	route := gin.Default()
	route.Use(gin.Logger())
	db := config.Init()

	userRepository := repository.NewRepository(db)

	userService := service.NewService(userRepository)
	authService := middleware.NewService()
	userHandler := handler.NewUserHandler(userService, authService)

	route.POST("/register", userHandler.Register)
	route.POST("/login", userHandler.Login)

	log.Fatal(route.Run(":3001"))
}
