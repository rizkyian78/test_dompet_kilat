package handler

import (
	"fmt"
	"net/http"
	"test_dompet/src/middleware"
	"test_dompet/src/model/users"
	"test_dompet/src/service"

	"github.com/gin-gonic/gin"
)

type userHandler struct {
	userService service.Service
	authService middleware.Service
}

func NewUserHandler(userService service.Service, authService middleware.Service) *userHandler {
	return &userHandler{userService, authService}
}

func (h *userHandler) Register(c *gin.Context) {
	//catch user input
	var input users.RegisterInput
	err := c.ShouldBindJSON(&input)
	fmt.Println(input)
	if err != nil {

		errorMessage := gin.H{
			"error": "Input Error",
		}
		c.JSON(http.StatusBadRequest, errorMessage)
		return
	}
	user, err := h.userService.RegisterUser(input)
	if err != nil {
		errorMessage := gin.H{
			"error": "Failed to Register Student",
		}
		c.JSON(http.StatusBadRequest, errorMessage)
		return
	}
	userFormat := users.FormatUser(user)
	c.JSON(http.StatusOK, userFormat)
}

func (h *userHandler) Login(c *gin.Context) {
	//catch user input
	var input users.LoginInput
	err := c.ShouldBindJSON(&input)
	fmt.Println(input)
	if err != nil {

		errorMessage := gin.H{
			"error": "Input Error",
		}
		c.JSON(http.StatusBadRequest, errorMessage)
		return
	}
	user, err := h.userService.LoginUser(input)
	if err != nil {
		errorMessage := gin.H{
			"error": "Email and Password not Found",
		}
		c.JSON(http.StatusBadRequest, errorMessage)
		return
	}
	token, err := h.authService.GenerateToken(user.ID)
	if err != nil {
		errorMessage := gin.H{
			"error": "Internal Error",
		}
		c.JSON(http.StatusBadRequest, errorMessage)
		return
	}
	newFormat := users.LoginFormatter{
		ID:       user.ID,
		Email:    user.Email,
		Username: user.Username,
		Token:    token,
	}
	userFormat := users.LoginFormatterResponse(newFormat)
	c.JSON(http.StatusOK, userFormat)
}
